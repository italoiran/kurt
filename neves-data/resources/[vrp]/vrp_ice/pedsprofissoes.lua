local pedlist = {
	{ ['x'] = -1886.74, ['y'] = 2049.91, ['z'] = 140.99, ['h'] = 162.23, ['hash'] = 0xC8BB1E52, ['hash2'] = "u_m_y_mani" },
	{ ['x'] = 78.92, ['y'] = 112.27, ['z'] = 81.17, ['h'] = 163.57, ['hash'] = 0x999B00C6, ['hash2'] = "ig_paper" },
	{ ['x'] = 223.35, ['y'] = 121.76, ['z'] = 102.84, ['h'] = 251.32, ['hash'] = 0xCDEF5408, ['hash2'] = "mp_s_m_armoured_01" },
	{ ['x'] = -563.46, ['y'] = 284.61, ['z'] = 85.38, ['h'] = 342.49, ['hash'] = 0xAD9EF1BB, ['hash2'] = "s_m_o_busker_01" },
	{ ['x'] = 719.33, ['y'] = 152.26, ['z'] = 80.76, ['h'] = 152.69, ['hash'] = 0x62018559, ['hash2'] = "s_m_y_airworker" },
	{ ['x'] = -151.59, ['y'] = -1622.95, ['z'] = 33.66, ['h'] = 234.68, ['hash'] = 0xFAB48BCB, ['hash2'] = "a_f_m_fatbla_01" },
	{ ['x'] = 84.12, ['y'] = -1966.76, ['z'] = 20.94, ['h'] = 231.45, ['hash'] = 0xFAB48BCB, ['hash2'] = "a_f_m_fatbla_01" },
	{ ['x'] = -1120.12, ['y'] = -1625.36, ['z'] = 4.4, ['h'] = 307.75, ['hash'] = 0x303638A7, ['hash2'] = "a_f_m_beach_01" },
	{ ['x'] = 1310.87, ['y'] = -1697.65, ['z'] = 57.84, ['h'] = 188.87, ['hash'] = 0xD172497E, ['hash2'] = "a_m_m_afriamer_01" },
	{ ['x'] = 361.47, ['y'] = -2042.82, ['z'] = 22.36, ['h'] = 142.34, ['hash'] = 0xFAB48BCB, ['hash2'] = "a_f_m_fatbla_01" }
}

Citizen.CreateThread(function()
	for k,v in pairs(pedlist) do
		RequestModel(GetHashKey(v.hash2))
		while not HasModelLoaded(GetHashKey(v.hash2)) do
			Citizen.Wait(10)
		end

		local ped = CreatePed(4,v.hash,v.x,v.y,v.z-1,v.h,false,true)
		FreezeEntityPosition(ped,true)
		SetEntityInvincible(ped,true)
		SetBlockingOfNonTemporaryEvents(ped,true)
	end
end)