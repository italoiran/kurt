-----------------------------------------------------------------------------------------------------------------------------------------
-- DENSITY NPCS
-----------------------------------------------------------------------------------------------------------------------------------------
local density = false
Citizen.CreateThread(function()
	SetTimeout(120000,function()
		density = true
	end)
end)

Citizen.CreateThread(function()
	while true do
		local idle = 500
		if not density then
			idle = 4
			SetVehicleDensityMultiplierThisFrame(0.0)
			SetSomeVehicleDensityMultiplierThisFrame(0.0)
			SetRandomVehicleDensityMultiplierThisFrame(0.0)
			SetParkedVehicleDensityMultiplierThisFrame(0.0)
			SetScenarioPedDensityMultiplierThisFrame(1.5,1.5)
			SetPedDensityMultiplierThisFrame(1.5)
		else
			if IsPedSittingInAnyVehicle(PlayerPedId()) then
				idle = 4
				if GetPedInVehicleSeat(GetVehiclePedIsIn(PlayerPedId()),-1) == PlayerPedId() then
					idle = 4
					SetVehicleDensityMultiplierThisFrame(0.0)
					SetRandomVehicleDensityMultiplierThisFrame(0.0)
					SetParkedVehicleDensityMultiplierThisFrame(0.0)
				else
					SetVehicleDensityMultiplierThisFrame(0.0)
					SetRandomVehicleDensityMultiplierThisFrame(0.0)
					SetParkedVehicleDensityMultiplierThisFrame(0.0)
				end
			else
				SetParkedVehicleDensityMultiplierThisFrame(0.0)
				SetVehicleDensityMultiplierThisFrame(0.0)
			end
			SetScenarioPedDensityMultiplierThisFrame(1.5,1.5)
			SetPedDensityMultiplierThisFrame(1.5)
		end
		Citizen.Wait(idle)
	end
end)

Citizen.CreateThread(function() 
	while true do
		local idle = 3000
		local ped = PlayerPedId()
		if GetEntityHealth(ped) <= 199 then
			idle = 5
			if GetSelectedPedWeapon(ped) == GetHashKey("WEAPON_UNARMED") then
				DisablePlayerFiring(PlayerId(), true)
				DisableControlAction(0,45,true)
				-- DisableControlAction(0,80,true)
				DisableControlAction(0,140,true)
			end
		end
		Citizen.Wait(idle)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- DISPATCH
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
	for _, i in ipairs(GetActivePlayers()) do
		EnableDispatchService(i,false)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- DESABILITAR A CORONHADA
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
        local idle = 500
        local ped = PlayerPedId()
		if IsPedArmed(ped,6) then
			idle = 4
			DisableControlAction(0,140,true)
			DisableControlAction(0,141,true)
			DisableControlAction(0,142,true)
		end
		Citizen.Wait(idle)
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- REMOVER ARMA ABAIXO DE 40MPH DENTRO DO CARRO
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
	while true do
		local idle = 500
		local ped = PlayerPedId()
		if IsPedInAnyVehicle(ped) then
			idle = 4
			local vehicle = GetVehiclePedIsIn(PlayerPedId(), false)
			local speed = GetEntitySpeed(vehicle)*3.6
			if GetEntityModel(vehicle) ~= GetHashKey('buzzard2') and GetEntityModel(vehicle) ~= GetHashKey('maverick2') then
				idle = 4
				if speed >= 40 or GetSelectedPedWeapon(ped) ~= GetHashKey("WEAPON_STUNGUN") then
					idle = 4
					DisableControlAction(0, 69, true)
					DisableControlAction(0, 92, true)
				end
			end
		end
		Citizen.Wait(idle)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- DRIFT
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
	while true do
		local idle = 500
		local ped = PlayerPedId()
		local vehicle = GetVehiclePedIsIn(PlayerPedId())
		if IsPedInAnyVehicle(ped) then
			local speed = GetEntitySpeed(vehicle)*3.6
			idle = 4
			if GetPedInVehicleSeat(vehicle,-1) == ped 
				and (GetEntityModel(vehicle) ~= GetHashKey("coach") 
				and GetEntityModel(vehicle) ~= GetHashKey("bus") 
				and GetEntityModel(vehicle) ~= GetHashKey("youga2") 
				and GetEntityModel(vehicle) ~= GetHashKey("ratloader") 
				and GetEntityModel(vehicle) ~= GetHashKey("taxi") 
				and GetEntityModel(vehicle) ~= GetHashKey("boxville4") 
				and GetEntityModel(vehicle) ~= GetHashKey("trash2") 
				and GetEntityModel(vehicle) ~= GetHashKey("tiptruck") 
				and GetEntityModel(vehicle) ~= GetHashKey("rebel") 
				and GetEntityModel(vehicle) ~= GetHashKey("speedo") 
				and GetEntityModel(vehicle) ~= GetHashKey("phantom") 
				and GetEntityModel(vehicle) ~= GetHashKey("packer") 
				and GetEntityModel(vehicle) ~= GetHashKey("paramedicoambu")) then
				if speed <= 100.0 then
					if IsControlPressed(1,21) then
						SetVehicleReduceGrip(vehicle,true)
					else
						SetVehicleReduceGrip(vehicle,false)
					end
				end    
			end
		end
		Citizen.Wait(idle)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- PERDER A ADERÊNCIA QUANDO ESTIVER COM A RODA ESTOURADA
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
	while true do
		local idle = 500
		local ped = PlayerPedId()
		local vehicle = GetVehiclePedIsIn(PlayerPedId())
		if IsPedInAnyVehicle(ped) then
			idle = 4
			if GetPedInVehicleSeat(vehicle,-1) == ped then
				for i = 0, 5 do
					if IsVehicleTyreBurst(vehicle, i, true) then
						SetVehicleReduceGrip(vehicle,true)
					end
				end   
			end
		end
		Citizen.Wait(idle)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- TASERTIME
-----------------------------------------------------------------------------------------------------------------------------------------
local tasertime = false
Citizen.CreateThread(function()
	while true do
		local idle = 500
		local ped = PlayerPedId()
		if IsPedBeingStunned(ped) then
			idle = 4
			SetPedToRagdoll(ped,10000,10000,0,0,0,0)
		end

		if IsPedBeingStunned(ped) and not tasertime then
			tasertime = true
			idle = 4
			SetTimecycleModifier("REDMIST_blend")
			ShakeGameplayCam("FAMILY5_DRUG_TRIP_SHAKE",1.0)
		elseif not IsPedBeingStunned(ped) and tasertime then
			tasertime = false
			SetTimeout(5000,function()
				SetTimecycleModifier("hud_def_desat_Trevor")
				SetTimeout(10000,function()
					SetTimecycleModifier("")
					SetTransitionTimecycleModifier("")
					StopGameplayCamShaking()
				end)
			end)
		end
		
		Citizen.Wait(idle)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- BLACKLIST WEAPONS
-----------------------------------------------------------------------------------------------------------------------------------------
blackWeapons = {
	"WEAPON_PISTOL50",
	"WEAPON_SNSPISTOL_MK2",
    "WEAPON_SNSPISTOL",
	"WEAPON_HEAVYPISTOL",
	"WEAPON_FLAREGUN",
	"WEAPON_MARKSMANPISTOL",
	"WEAPON_REVOLVER",
	"WEAPON_REVOLVER_MK2",
	"WEAPON_DOUBLEACTION",
	"WEAPON_RAYPISTOL",
--	"WEAPON_SMG_MK2",
--	"WEAPON_PUMPSHOTGUN_MK2",
--	"WEAPON_MACHINEPISTOL",
--	"WEAPON_COMBATPDW",
	"WEAPON_MINISMG",
	"WEAPON_RAYCARBINE",
--	"WEAPON_PUMPSHOTGUN",
--	"WEAPON_SAWNOFFSHOTGUN",
	"WEAPON_ASSAULTSHOTGUN",
	"WEAPON_BULLPUPSHOTGUN",
	"WEAPON_HEAVYSHOTGUN",
	"WEAPON_DBSHOTGUN",
	"WEAPON_AUTOSHOTGUN",
	"WEAPON_VINTAGEPISTOL",
	"WEAPON_GUSENBERG",
	"WEAPON_ASSAULTRIFLE",
	"WEAPON_MUSKET",
	"WEAPON_PISTOL",
	--"WEAPON_PISTOL_MK2",
	--"WEAPON_ASSAULTRIFLE_MK2",
	--"WEAPON_CARBINERIFLE_MK2",
	"WEAPON_ADVANCEDRIFLE",
--	"WEAPON_SPECIALCARBINE",
--	"WEAPON_SPECIALCARBINE_MK2",
	"WEAPON_BULLPUPRIFLE",
	"WEAPON_BULLPUPRIFLE_MK2",
	"WEAPON_COMPACTRIFLE",
	"WEAPON_MG",
	"WEAPON_COMBATMG",
	"WEAPON_COMBATMG_MK2",
	"WEAPON_SNIPERRIFLE",
	"WEAPON_HEAVYSNIPER",
	"WEAPON_HEAVYSNIPER_MK2",
	"WEAPON_MARKSMANRIFLE",
	"WEAPON_MARKSMANRIFLE_MK2",
	"WEAPON_RPG",
	"WEAPON_GRENADELAUNCHER",
	"WEAPON_GRENADELAUNCHER_SMOKE",
	"WEAPON_MINIGUN",
	"WEAPON_FIREWORK",
	"WEAPON_RAILGUN",
	"WEAPON_HOMINGLAUNCHER",
	"WEAPON_COMPACTLAUNCHER",
	"WEAPON_RAYMINIGUN",
	"WEAPON_GRENADE",
	"WEAPON_BZGAS",
	"WEAPON_MOLOTOV",
	"WEAPON_STICKYBOMB",
	"WEAPON_PROXMINE",
	"WEAPON_PIPEBOMB",
	"WEAPON_SNOWBALL",
	"WEAPON_BALL",
	"WEAPON_SMOKEGRENADE"
}


Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1000)
		for k,v in ipairs(blackWeapons) do
			if GetSelectedPedWeapon(PlayerPedId()) == GetHashKey(v) then
				TriggerServerEvent("adminLogs:Armamentos", v)
				RemoveWeaponFromPed(PlayerPedId(),GetHashKey(v))
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- DESABILITAR X NA MOTO
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
        local idle = 500
        local ped = PlayerPedId()
		if IsPedInAnyVehicle(ped) then
            local vehicle = GetVehiclePedIsIn(ped)
			if (GetPedInVehicleSeat(vehicle, -1) == ped or GetPedInVehicleSeat(vehicle, 0) == ped) and GetVehicleClass(vehicle) == 8 then
				idle = 4
                DisableControlAction(0, 345, true)
            end
		end
		Citizen.Wait(idle)
    end
end)
-- DESATIVA O ROUBO DO VEÍCULO SEGURANDO F [ CAR JACKING ]
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
    local idle = 500
    local ped = PlayerPedId()
	if IsPedJacking(ped) then
		idle = 4
      local veh = GetVehiclePedIsIn(ped)
      SetPedIntoVehicle(ped, veh, 0)
      ClearPedTasks(ped)
		end
		Citizen.Wait(idle)
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- DAMAGE WALK MODE
-----------------------------------------------------------------------------------------------------------------------------------------
local hurt = false
Citizen.CreateThread(function()
	while true do
		local delayThread = 500
		local ped = PlayerPedId()
		if not IsEntityInWater(ped) then
			if GetEntityHealth(ped) <= 199 then
				delayThread = 5
				setHurt()
			elseif hurt and GetEntityHealth(ped) > 200 then
				setNotHurt()
			end
		end
		Citizen.Wait(delayThread)
	end
end)

function setHurt()
    hurt = true
    RequestAnimSet("move_m@injured")
    SetPedMovementClipset(PlayerPedId(),"move_m@injured",true)
	SetPlayerHealthRechargeMultiplier(PlayerId(),0.0)
	DisableControlAction(0,21) 
	DisableControlAction(0,22)
end

function setNotHurt()
    hurt = false
	SetPlayerHealthRechargeMultiplier(PlayerId(),0.0)
    ResetPedMovementClipset(PlayerPedId())
    ResetPedWeaponMovementClipset(PlayerPedId())
    ResetPedStrafeClipset(PlayerPedId())
end
-----------------------------------------------------------------------------------------------------------------------------------------
---ESTOURAR OS PNEUS QUANDO CAPOTA
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(5)
        local ped = PlayerPedId()
        if IsPedInAnyVehicle(ped) then
            local vehicle = GetVehiclePedIsIn(ped)
            if GetPedInVehicleSeat(vehicle,-1) == ped then
                local roll = GetEntityRoll(vehicle)
                if (roll > 75.0 or roll < -75.0) and GetEntitySpeed(vehicle) < 2 then
                      if IsVehicleTyreBurst(vehicle, wheel_rm1, 0) == false then
                    SetVehicleTyreBurst(vehicle, 0, 1)
                    Citizen.Wait(100)
                    SetVehicleTyreBurst(vehicle, 1, 1)
                    Citizen.Wait(100)
                    SetVehicleTyreBurst(vehicle, 2, 1)
                    Citizen.Wait(100)
                    SetVehicleTyreBurst(vehicle, 3, 1)
                    Citizen.Wait(100)
                    SetVehicleTyreBurst(vehicle, 4, 1)
                    Citizen.Wait(100)
                    SetVehicleTyreBurst(vehicle, 5, 1)
                    Citizen.Wait(100)
                    SetVehicleTyreBurst(vehicle, 45, 1)
                    Citizen.Wait(100)
                    SetVehicleTyreBurst(vehicle, 47, 1)
                    end
                end
            end
        end
    end
end)

-----------------------------------------------------------------------------------------------------------------------------------------
-- DESABILITAR AUTO-CAPACETE NA MOTO
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread( function()
    while true do
        Citizen.Wait(5)  
        local veh = GetVehiclePedIsUsing(PlayerPedId())
        if veh ~= 0 then 
            SetPedConfigFlag(PlayerPedId(),35,false) 
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- [ REMOVER O Q ]-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(1)
        local ped = PlayerPedId()
        local health = GetEntityHealth(ped)
        if health >= 101 then
        DisableControlAction(0,44,true)
 	        end
    end
end)