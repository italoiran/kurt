-------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------Lax Academy.----------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------

data = {
  ["weapon"] = "WEAPON_COMBATPISTOL",
  			   "WEAPON_PISTOL_MK2","WEAPON_SMG","WEAPON_SMG_MK2","WEAPON_KNiFE","WEAPON_MACHINEPISTOL","WEAPON_ASSAULTRIFLE","WEAPON_ASSAULTRIFLE_MK2","WEAPON_CARBINERIFLE","WEAPON_SPECIALCARBINE","WEAPON_SPECIALCARBINE_MK2","WEAPON_CARBINERIFLE_MK2","weapon_combatpdw","weapon_dagger","weapon_machete","weapon_nightstick", -- The weapon you must have to trigger the animation
  ["peds"] = {
    ["mp_m_freemode_01"] = {
      ["components"] = {
        [7] = {
          [1] = 3,
          [6] = 5,
          [8] = 2,
          [42] = 43,
          [110] = 111,
          [119] = 120
        },
        [8] = { 
          [16] = 18
        }
      }
    },
    ["mp_f_freemode_01"] = {
      ["components"] = {
        [7] = {
          [1] = 3,
          [6] = 5,
          [8] = 2,
          [29] = 30,
          [81] = 82
        },
        [8] = {
          [9] = 10
        }
      }
    },
  }
}