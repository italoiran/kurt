-----------------------------------------------------------------------------------------------------------------------------------------
-- NOTIFY CSS,PREFIX,MENSAGEM,TEMPO
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterNetEvent("Notify")
AddEventHandler("Notify",function(css,prefix,message,delay)
	if message == nil then
		message = prefix
		prefix = string.upper(css)
	end

	if not delay then delay = 9000 end
	SendNUIMessage({ css = css, prefix = prefix, message = message, delay = delay })
end)

--[[RegisterCommand("teste",function(source,args)
	TriggerEvent('Notify', 'sucesso', 'Sucesso',"Sucesso")
	TriggerEvent('Notify', 'negado', 'Negado',"Negado")
	TriggerEvent('Notify', 'importante','Importante', "Importante")	
	TriggerEvent('Notify', 'aviso', 'Aviso',"Aviso")	
	TriggerEvent('Notify', 'financeiro',"Financeiro","Financeiro")	
end)]]--