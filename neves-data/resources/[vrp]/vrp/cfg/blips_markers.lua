local cfg = {}

cfg.blips = {
	{ 25.65,-1346.58,29.49,52,4,"Loja de Departamentos",0.5 },
	{ 2556.75,382.01,108.62,52,4,"Loja de Departamentos",0.5 },
	{ 1163.54,-323.04,69.20,52,4,"Loja de Departamentos",0.5 },
	{ -707.37,-913.68,19.21,52,4,"Loja de Departamentos",0.5 },
	{ -47.73,-1757.25,29.42,52,4,"Loja de Departamentos",0.5 },
	{ 373.90,326.91,103.56,52,4,"Loja de Departamentos",0.5 },
	{ -3243.10,1001.23,12.83,52,4,"Loja de Departamentos",0.5 },
	{ 1729.38,6415.54,35.03,52,4,"Loja de Departamentos",0.5 },
	{ 547.90,2670.36,42.15,52,4,"Loja de Departamentos",0.5 },
	{ 1960.75,3741.33,32.34,52,4,"Loja de Departamentos",0.5 },
	{ 2677.90,3280.88,55.24,52,4,"Loja de Departamentos",0.5 },
	{ 1698.45,4924.15,42.06,52,4,"Loja de Departamentos",0.5 },
	{ -1820.93,793.18,138.11,52,4,"Loja de Departamentos",0.5 },
	{ 1392.46,3604.95,34.98,52,4,"Loja de Departamentos",0.5 },
	{ -2967.82,390.93,15.04,52,4,"Loja de Departamentos",0.5 },
	{ -3040.10,585.44,7.90,52,4,"Loja de Departamentos",0.5 },
	{ 1135.56,-982.20,46.41,52,4,"Loja de Departamentos",0.5 },
	{ 1165.91,2709.41,38.15,52,4,"Loja de Departamentos",0.5 },
	{ -1487.18,-379.02,40.16,52,4,"Loja de Departamentos",0.5 },
	{ -1222.78,-907.22,12.32,52,4,"Loja de Departamentos",0.5 },
	
	{ 1395.19,3613.61,34.98,59,4,"Loja de Bebidas",0.5 },
	{ -2959.60,387.12,14.04,59,4,"Loja de Bebidas",0.5 },
	{ -3047.81,585.68,7.90,59,4,"Loja de Bebidas",0.5 },
	{ 1126.89,-980.64,45.41,59,4,"Loja de Bebidas",0.5 },
	{ 1168.95,2717.74,37.15,59,4,"Loja de Bebidas",0.5 },
	{ -1479.08,-375.34,39.16,59,4,"Loja de Bebidas",0.5 },
	{ -1220.86,-915.88,11.32,59,4,"Loja de Bebidas",0.5 },
	
	

	{ 285.37,-587.86,43.37,80,1,"Hospital",0.4 },

	{ 75.40,-1392.92,29.37,73,4,"Loja de Roupas",0.5 },
	{ -709.40,-153.66,37.41,73,4,"Loja de Roupas",0.5 },
	{ -163.20,-302.03,39.73,73,4,"Loja de Roupas",0.5 },
	{ 425.58,-806.23,29.49,73,4,"Loja de Roupas",0.5 },
	{ -822.34,-1073.49,11.32,73,4,"Loja de Roupas",0.5 },
	{ -1193.81,-768.49,17.31,73,4,"Loja de Roupas",0.5 },
	{ -1450.85,-238.15,49.81,73,4,"Loja de Roupas",0.5 },
	{ 4.90,6512.47,31.87,73,4,"Loja de Roupas",0.5 },
	{ 1693.95,4822.67,42.06,73,4,"Loja de Roupas",0.5 },
	{ 126.05,-223.10,54.55,73,4,"Loja de Roupas",0.5 },
	{ 614.26,2761.91,42.08,73,4,"Loja de Roupas",0.5 },
	{ 1196.74,2710.21,38.22,73,4,"Loja de Roupas",0.5 },
	{ -3170.18,1044.54,20.86,73,4,"Loja de Roupas",0.5 },
	{ -1101.46,2710.57,19.10,73,4,"Loja de Roupas",0.5 },

	{ 1692.62,3759.50,34.70,76,17,"Loja de Armamentos",0.4 },
	{ 252.89,-49.25,69.94,76,17,"Loja de Armamentos",0.4 },
	{ 843.28,-1034.02,28.19,76,17,"Loja de Armamentos",0.4 },
	{ -331.35,6083.45,31.45,76,17,"Loja de Armamentos",0.4 },
	{ -663.15,-934.92,21.82,76,17,"Loja de Armamentos",0.4 },
	{ -1305.18,-393.48,36.69,76,17,"Loja de Armamentos",0.4 },
	{ -1118.80,2698.22,18.55,76,17,"Loja de Armamentos",0.4 },
	{ 2568.83,293.89,108.73,76,17,"Loja de Armamentos",0.4 },
	{ -3172.68,1087.10,20.83,76,17,"Loja de Armamentos",0.4 },
	{ 21.32,-1106.44,29.79,76,17,"Loja de Armamentos",0.4 },
	{ 811.19,-2157.67,29.61,76,17,"Loja de Armamentos",0.4 },

	{ -815.59,-182.16,37.56,71,4,"Salão de Beleza",0.5 },
	{ 139.21,-1708.96,29.30,71,4,"Salão de Beleza",0.5 },
	{ -1282.00,-1118.86,7.00,71,4,"Salão de Beleza",0.5 },
	{ 1934.11,3730.73,32.85,71,4,"Salão de Beleza",0.5 },
	{ 1211.07,-475.00,66.21,71,4,"Salão de Beleza",0.5 },
	{ -34.97,-150.90,57.08,71,4,"Salão de Beleza",0.5 },
	{ -280.37,6227.01,31.70,71,4,"Salão de Beleza",0.5 },

	{ 235.12,216.84,106.28,207,0,"Banco",0.5 },
	{ -1213.44,-331.02,37.78,207,0,"Banco",0.5 }, 
	{ -351.59,-49.68,49.04,207,0,"Banco",0.5 },
	{ 313.47,-278.81,54.17,207,0,"Banco",0.5 },
	{ 149.35,-1040.53,29.37,207,0,"Banco",0.5 },
	{ -2962.60,482.17,15.70,207,0,"Banco",0.5 },
	{ -112.81,6469.91,31.62,207,0,"Banco",0.5 },
	{ 1175.74,2706.80,38.09,207,0,"Banco",0.5 },

	{ 1322.64,-1651.97,52.27,75,4,"Tatuagens",0.4 },
	{ -1153.67,-1425.68,4.95,75,4,"Tatuagens",0.4 },
	{ 322.13,180.46,103.58,75,4,"Tatuagens",0.4 },
	{ -3170.07,1075.05,20.82,75,4,"Tatuagens",0.4 },
	{ 1864.63,3747.73,33.03,75,4,"Tatuagens",0.4 },
	{ -293.71,6200.04,31.48,75,4,"Tatuagens",0.4 },

	{ -29.99,-1105.07,26.43,225,4,"Concessionária",0.5 },
	{ 437.61,-982.04,30.69,60,3,"Departamento Policial",0.5 },
    { 1851.45,3686.71,34.26,60,3,"Departamento Policial",0.5 },
    { -448.18,6011.68,31.71,60,3,"Departamento Policial",0.5 },
	{ 55.43,-876.19,30.66,357,3,"Garagem",0.4 },
	{ 317.25,2623.14,44.46,357,3,"Garagem",0.4 },
	{ -773.34,5598.15,33.60,357,3,"Garagem",0.4 },
	{ 596.40,90.65,93.12,357,3,"Garagem",0.4 },
	{ -340.76,265.97,85.67,357,3,"Garagem",0.4 },
	{ -2030.01,-465.97,11.60,357,3,"Garagem",0.4 },
	{ -1184.92,-1510.00,4.64,357,3,"Garagem",0.4 },
	{ -73.44,-2004.99,18.27,357,3,"Garagem",0.4 },
	{ 214.02,-808.44,31.01,357,3,"Garagem",0.4 },
	{ -348.88,-874.02,31.31,357,3,"Garagem",0.4 },
	{ 67.74,12.27,69.21,357,3,"Garagem",0.4 },
	{ 361.90,297.81,103.88,357,3,"Garagem",0.4 },
	{ 1156.90,-453.73,66.98,357,3,"Garagem",0.4 },
	{ -102.21,6345.18,31.57,357,3,"Garagem",0.4 },
	{ -830.74,-420.58,36.77,357,3,"Garagem",0.4 },
	{ 1990.01,3052.29,47.22,385,5,"Yellow Jack",0.5 },
	{ 128.96,-1299.03,29.24,121,7,"Vanilla Unicorn",0.5 },
	{ 265.05,-1262.65,29.3,361,41,"Posto de Gasolina",0.4 },
	{ 819.02,-1027.96,26.41,361,41,"Posto de Gasolina",0.4 },
	{ 1208.61,-1402.43,35.23,361,41,"Posto de Gasolina",0.4 },
	{ 1181.48,-330.26,69.32,361,41,"Posto de Gasolina",0.4 },
	{ 621.01,268.68,103.09,361,41,"Posto de Gasolina",0.4 },
	{ 2581.09,361.79,108.47,361,41,"Posto de Gasolina",0.4 },
	{ 175.08,-1562.12,29.27,361,41,"Posto de Gasolina",0.4 },
	{ -319.76,-1471.63,30.55,361,41,"Posto de Gasolina",0.4 },
	{ 1782.33,3328.46,41.26,361,41,"Posto de Gasolina",0.4 },
	{ 49.42,2778.8,58.05,361,41,"Posto de Gasolina",0.4 },
	{ 264.09,2606.56,44.99,361,41,"Posto de Gasolina",0.4 },
	{ 1039.38,2671.28,39.56,361,41,"Posto de Gasolina",0.4 },
	{ 1207.4,2659.93,37.9,361,41,"Posto de Gasolina",0.4 },
	{ 2539.19,2594.47,37.95,361,41,"Posto de Gasolina",0.4 },
	{ 2679.95,3264.18,55.25,361,41,"Posto de Gasolina",0.4 },
	{ 2005.03,3774.43,32.41,361,41,"Posto de Gasolina",0.4 },
	{ 1687.07,4929.53,42.08,361,41,"Posto de Gasolina",0.4 },
	{ 1701.53,6415.99,32.77,361,41,"Posto de Gasolina",0.4 },
	{ 180.1,6602.88,31.87,361,41,"Posto de Gasolina",0.4 },
	{ -94.46,6419.59,31.48,361,41,"Posto de Gasolina",0.4 },
	{ -2555.17,2334.23,33.08,361,41,"Posto de Gasolina",0.4 },
	{ -1800.09,803.54,138.72,361,41,"Posto de Gasolina",0.4 },
	{ -1437.0,-276.8,46.21,361,41,"Posto de Gasolina",0.4 },
	{ -2096.3,-320.17,13.17,361,41,"Posto de Gasolina",0.4 },
	{ -724.56,-935.97,19.22,361,41,"Posto de Gasolina",0.4 },
	{ -525.26,-1211.19,18.19,361,41,"Posto de Gasolina",0.4 },
	{ -70.96,-1762.21,29.54,361,41,"Posto de Gasolina",0.4 },
	{ 133.15,-1462.67,29.36,267,2,"Emprego | Entregador",0.4 },
	{ 223.83,121.52,102.78,67,2,"Emprego | Transportador",0.5 },
	{ -1880.48,2041.93,140.73,478,27,"Emprego | Vinhedo",0.5 },
	{ -470.8,-1718.2,18.69,171,2,"Emprego | Lixeiro",0.5 },
	{ 455.15,-601.45,28.53,513,2,"Emprego | Motoristas",0.5 },
	{ 78.85,112.18,81.17,537,2,"Emprego | Carteiro",0.5 },
	{ -358.0,-133.88,38.8,402,47,"Central | Mecânica",0.5 }
}

return cfg