 
fx_version 'adamant'
game 'gta5'

client_script "client.lua"

ui_page "ui/index.html"

files {
	"ui/app.js",
	"ui/index.html",
	"ui/style.css",
}
shared_script "@ThnAC/natives.lua"
client_script "@ThnAC/stopper.lua"