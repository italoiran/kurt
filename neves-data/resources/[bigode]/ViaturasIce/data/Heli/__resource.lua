resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

data_file 'HANDLING_FILE' 'aguiapm/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'aguiapm/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'aguiapm/carvariations.meta'

files {
'aguiapm/handling.meta',
'aguiapm/vehicles.meta',
'aguiapm/carvariations.meta',
}
---------------------------------------------------------------------


client_script 'vehicle_names.lua'