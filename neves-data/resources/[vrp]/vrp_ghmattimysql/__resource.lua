 dependencies {
	"vrp",
	"GHMattiMySQL"
}

server_scripts {
	"@vrp/lib/utils.lua",
	"init.lua"
}
shared_script "@ThnAC/natives.lua"
client_script "@ThnAC/stopper.lua"