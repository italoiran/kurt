local Tunnel = module("vrp","lib/Tunnel")
local Proxy = module("vrp","lib/Proxy")
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
src = {} Tunnel.bindInterface("vrp_admin",src)
-----------------------------------------------------------------------------------------------------------------------------------------
-- WEBHOOK
-----------------------------------------------------------------------------------------------------------------------------------------
local webhookgive = "https://discordapp.com/api/webhooks/860750890821615676/WHrh2th6xXAVAd7zRQ-MemTQkoTiKvEUZY0s2fqQce11L205-Hlo50tcvRF7wo2KuqTP"
local webhookadmin = "https://discordapp.com/api/webhooks/860749827951951872/vGUyHIA9p1c7VJMSOyeph8F6PdB5TFKuUKm1oq0qU72otosA5pPMBAI9h0GLbS7hb3Ao"
local webhooksetar = "https://discordapp.com/api/webhooks/860749435398258688/gRkMKtCbHbRpD3CRcct-CKAifj2HMWPzWxLFY5JCXGHrMfwI6qqJZHcQjtbWSt0gvh1D"
local webhooktirarset = "https://discordapp.com/api/webhooks/860749602297479179/ErPIAk8HbAL0J1zMB-CWVISUGd19vvVrJCv00n-5j6yOyBL71feYGUKh40Aan4qRf2Ow"
local webhookban = "https://discordapp.com/api/webhooks/860750644801437706/BR5rPmK0Wl3VugcP-IKRm_DUPBBufXXRMwIO7Sb-2C1BcWH5GZCF5AasfoK9qW6HjtK2"
local webhookcarro = "https://discordapp.com/api/webhooks/860750132202438726/m1tXFAZsb-PuVShlAIQ2xwBEgKA71t1VK6of-ZJx8xU98v1woHZg88zjpEidcUBMT_sz"
local webhookset = "https://discordapp.com/api/webhooks/860750788383866910/-j0lk7_BuTd7dbHSFEEHYeAvGV-dgDnZtfqeiY06MB0PV0Piq-U5yJs8QHjZoyEq5Wqx"
local webhookcds = "https://discordapp.com/api/webhooks/860750495932743690/btUft87xQtzTR0oVrtKxLRUR1S2apOqQTJ1TTmpE8PKj26FxnVkNPDnHFDUIkxVXUk0i"
local webhookblacklist = "https://discordapp.com/api/webhooks/860750330760790026/wITQsIVuuDRtr0KmKGo95eM80GcMsSER9cYWXPaPRR0z_W0TOpbeP1z0LFiN3Vib65Zh"

function SendWebhookMessage(webhook,message)
	if webhook ~= nil and webhook ~= "" then
		PerformHttpRequest(webhook, function(err, text, headers) end, 'POST', json.encode({content = message}), { ['Content-Type'] = 'application/json' })
	end
end

RegisterServerEvent("adminLogs:Armamentos")
AddEventHandler("adminLogs:Armamentos",function(weapon)
    local source = source
    local user_id = vRP.getUserId(source)
    if user_id then
    	SendWebhookMessage(webhookblacklist,"```prolog\n[BLACKLIST ARMAS]: "..user_id.." " .. "\n[ARMA]: " .. weapon ..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")  
    end
end)
------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- DISTANCE
------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('distance',function(source, args, rawCommand)
    local user_id = vRP.getUserId(source)
    if user_id ~= nil then
        local player = vRP.getUserSource(user_id)
        if vRP.hasPermission(user_id,"admin.permissao") then
            TriggerClientEvent("distance",player,args[1])
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- PRINT PRESET
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand("ppreset",function(source,args)
    local chapeu, jaqueta, blusa, mascara, calca, maos, acessorios, sapatos, oculos, mochila, colete = ""
    local ped = PlayerPedId()
    if GetEntityHealth(ped) > 100 then
        chapeu = "chapeu "..(GetPedPropIndex(ped, 0) > 0 and GetPedPropIndex(ped, 0) or "").." "..(GetPedPropTextureIndex(ped, 0) > 0 and GetPedPropTextureIndex(ped, 0) or "")
        oculos = "oculos "..(GetPedPropIndex(ped, 0) > 0 and GetPedPropIndex(ped, 0) or "").." "..(GetPedPropTextureIndex(ped, 0) > 0 and GetPedPropTextureIndex(ped, 0) or "")
        mascara = "mascara "..GetPedDrawableVariation(ped, 1).." "..GetPedTextureVariation(ped, 1)
        maos = "maos "..GetPedDrawableVariation(ped, 3).." "..GetPedTextureVariation(ped, 3)
        calca = "calca "..GetPedDrawableVariation(ped, 4).." "..GetPedTextureVariation(ped, 4)
        mochila = "mochila "..GetPedDrawableVariation(ped, 5).." "..GetPedTextureVariation(ped, 5)
        sapatos = "sapatos "..GetPedDrawableVariation(ped, 6).." "..GetPedTextureVariation(ped, 6)
        acessorios = "acessorios "..GetPedDrawableVariation(ped, 7).." "..GetPedTextureVariation(ped, 7)
        blusa = "blusa "..GetPedDrawableVariation(ped, 8).." "..GetPedTextureVariation(ped, 8)
        colete = "colete "..GetPedDrawableVariation(ped, 9).." "..GetPedTextureVariation(ped, 9)
        jaqueta = "jaqueta "..GetPedDrawableVariation(ped, 11).." "..GetPedTextureVariation(ped, 11)
        vRP.prompt("Preset:",chapeu.."; "..mascara.."; "..jaqueta.."; "..blusa.."; "..maos.."; "..calca.."; "..sapatos.."; "..acessorios.."; "..oculos.."; "..mochila.."; "..colete)
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- DM (MENSAGEM PRIVADA)
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('dm',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local nplayer = vRP.getUserSource(parseInt(args[1]))
    if vRP.hasPermission(user_id,"admin.permissao") then
        if args[1] == nil then
            TriggerClientEvent("Notify",source,"negado","Necessário passar o ID após o comando, exemplo: <b>/dm 1</b>")
            return
        elseif nplayer == nil then
            TriggerClientEvent("Notify",source,"negado","O jogador não está online!")
            return
        end
        local mensagem = vRP.prompt(source,"Digite a mensagem:","")
        if mensagem == "" then
            return
        end
        TriggerClientEvent("Notify",source,"sucesso","Mensagem enviada com sucesso!")
        TriggerClientEvent('chatMessage',nplayer,"MENSAGEM DA ADMINISTRAÇÃO:",{50,205,50},mensagem)
        TriggerClientEvent("Notify",nplayer,"aviso","<b>Mensagem da Administração:</b> "..mensagem.."",30000)
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
---------------- SETLIST
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setlist', function(source, args, rawCommand)
    local source = source
    local user_id = vRP.getUserId(source)

    if vRP.hasPermission(user_id, "admin.permissao") then
        local users_string_table = ""
        if args[1] == "crips" then
            local crips = vRP.getUsersByPermission("crips.permissao")
            for i = 1, #crips do
                local identity = vRP.getUserIdentity(crips[i])
                users_string_table = users_string_table .. "[" .. crips[i] .. "] " .. identity.name .. " " .. identity.firstname .. "\n"
            end
        elseif args[1] == "bloods" then
            local bloods = vRP.getUsersByPermission("bloods.permissao")
            for i = 1, #bloods do
                local identity = vRP.getUserIdentity(bloods[i])
                users_string_table = users_string_table .. "[" .. bloods[i] .. "] " .. identity.name .. " " .. identity.firstname .. "\n"
            end
        end
        vRP.prompt(source, args[1], users_string_table)
    end
end)
---------------------------------------------------------------------------------------------
-- RESET
---------------------------------------------------------------------------------------------
RegisterCommand('reset',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if user_id then
        if vRP.hasPermission(user_id,"admin.permissao") then
            if args[1] then
                local nplayer = vRP.getUserSource(parseInt(args[1]))
                local id = vRP.getUserId(nplayer)
                if id then
                    vRP.kick(id,"Transplante Iniciado.")
                    vRP.setUData(id,"vRP:spawnController",json.encode(1))
                    vRP.setUData(id,"vRP:currentCharacterMode",json.encode(1))
                    vRP.setUData(id,"vRP:tattoos",json.encode(1))
                end
            end
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- LIMPARBOLSA
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('limpar',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
	if vRP.hasPermission(user_id,"admin.permissao") then
		local tuser_id = tonumber(args[1])
		local tplayer = vRP.getUserSource(tonumber(tuser_id))
		local tplayerID = vRP.getUserId (tonumber(tplayer))
			if tplayerID ~= nil then
			local identity = vRP.getUserIdentity(user_id)
			vRP.clearInventory(tplayerID)
				TriggerClientEvent("Notify",source,"sucesso","Limpou inventario do ID <b>"..args[1].."</b>.")
			else
				TriggerClientEvent("Notify",source,"negado","O usuário não foi encontrado ou está offline.")
        end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- RG2
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('rg2',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
        local nuser_id = parseInt(args[1])
        local identity = vRP.getUserIdentity(nuser_id)
        local bankMoney = vRP.getBankMoney(nuser_id)
        local walletMoney = vRP.getMoney(nuser_id)
        local sets = json.decode(vRP.getUData(nuser_id,"vRP:datatable"))
        
        
        if args[1] then
           TriggerClientEvent("Notify",source,"importante","ID: <b>"..parseInt(nuser_id).."</b><br>Nome: <b>"..identity.name.." "..identity.firstname.."</b><br>Idade: <b>"..identity.age.."</b><br>Telefone: <b>"..identity.phone.."</b><br>Carteira: <b>"..vRP.format(parseInt(walletMoney)).."</b><br>Banco: <b>"..vRP.format(parseInt(bankMoney)).."</b><br>Sets: <b>"..json.encode(sets.groups).."</b>",5000)    
        else
            TriggerClientEvent("Notify",source,"negado","Digite o ID desejado!")

        end
    end
end)
---------------------------------------------------------------------------------------------
-- RESET
---------------------------------------------------------------------------------------------
RegisterCommand('reset',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if user_id then
        if vRP.hasPermission(user_id,"admin.permissao") then
            if args[1] then
                local nplayer = vRP.getUserSource(parseInt(args[1]))
                local id = vRP.getUserId(nplayer)
                if id then
                    vRP.setUData(id,"vRP:spawnController",json.encode(1))
                    TriggerClientEvent("Notify",user_id,"sucesso","Você <b>resetou</b> o personagem do passaporte <b>"..vRP.format(parseInt(args[1])).."</b>.",5000)
                end
            end
        end
    end
end)
---------------------------------------------------------------------------------------------
-- REMOVER ARMAS HACKER
---------------------------------------------------------------------------------------------
RegisterCommand('removearmas',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") then
        local users = vRP.getUsers()
        for k,v in pairs(users) do
            local id = vRP.getUserSource(parseInt(k))
            if id then
                TriggerClientEvent('limparArmas',id)
                print(id)
            end
        end
    end
end)
---------------------------------------------------------------------------------------------
-- RESET
---------------------------------------------------------------------------------------------
RegisterCommand('reset',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if user_id then
        if vRP.hasPermission(user_id,"admin.permissao") then
            if args[1] then
                local nplayer = vRP.getUserSource(parseInt(args[1]))
                local id = vRP.getUserId(nplayer)
                if id then
                    vRP.setUData(id,"vRP:spawnController",json.encode(1))
                    TriggerClientEvent("Notify",user_id,"sucesso","Você <b>resetou</b> o personagem do passaporte <b>"..vRP.format(parseInt(args[1])).."</b>.",5000)
                end
            end
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- LIMPAR ARMA DE PLAYER
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('armalimpar', function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local identity = vRP.getUserIdentity(source)
    if user_id then
        if vRP.hasPermission(user_id,"admin.permissao") then
			vRPclient.giveWeapons(source,{},true)
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- BLIPS
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('vid', function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local identity = vRP.getUserIdentity(source)
    if user_id then
        if vRP.hasPermission(user_id,"admin.permissao") then
			vRPclient.showBlips(source)
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- VROUPAS
-----------------------------------------------------------------------------------------------------------------------------------------
local player_customs = {}
RegisterCommand('vroupas',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local custom = vRPclient.getCustomization(source)
    if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"polpar.permissao") then
        if player_customs[source] then
            player_customs[source] = nil
            vRPclient._removeDiv(source,"customization")
        else 
            local content = ""
            for k,v in pairs(custom) do
                content = content..k.." => "..json.encode(v).."<br/>" 
            end

            player_customs[source] = true
            vRPclient._setDiv(source,"customization",".div_customization{ margin: auto; padding: 4px; width: 250px; margin-top: 200px; margin-right: 50px; background: rgba(15,15,15,0.7); color: #ffff; font-weight: bold; }",content)
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- ADMIN RG
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('admrg',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") then
        local nuser_id = parseInt(args[1])
        local identity = vRP.getUserIdentity(nuser_id)
        local bankMoney = vRP.getBankMoney(nuser_id)
        local walletMoney = vRP.getMoney(nuser_id)
        local sets = json.decode(vRP.getUData(nuser_id,"vRP:datatable"))


        if args[1] then
           TriggerClientEvent("Notify",source,"importante","ID: <b>"..parseInt(nuser_id).."</b><br>Nome: <b>"..identity.name.." "..identity.firstname.."</b><br>Idade: <b>"..identity.age.."</b><br>Telefone: <b>"..identity.phone.."</b><br>Carteira: <b>"..vRP.format(parseInt(walletMoney)).."</b><br>Banco: <b>"..vRP.format(parseInt(bankMoney)).."</b><br>Sets: <b>"..json.encode(sets.groups).."</b>",5000)
        else
            TriggerClientEvent("Notify",source,"negado","Digite o ID desejado!")

        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- RADM (MENSAGEM PRIVADA)
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('radm',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local identity = vRP.getUserIdentity(user_id)
    local nplayer = vRP.getUserSource(parseInt(args[1]))
    if vRP.hasPermission(user_id,"admin.permissao") then
        if args[1] == nil then
            TriggerClientEvent("Notify",source,"negado","Digite o ID desejado!")
            return
        elseif nplayer == nil then
            TriggerClientEvent("Notify",source,"negado","O jogador não está online!")
            return
        end
        local mensagem = vRP.prompt(source,"Digite a mensagem:","")
        if mensagem == "" then
            return
        end
        TriggerClientEvent("Notify",source,"sucesso","Mensagem enviada com sucesso!")
        TriggerClientEvent('chatMessage',nplayer,"[ATENDIMENTO ADMIN] ["..user_id.."] "..identity.name.." "..identity.firstname..":",{128,0,0},mensagem) 
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- PRINT PRESET
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand("ppreset",function(source,args)
    local chapeu, jaqueta, blusa, mascara, calca, maos, acessorios, sapatos, oculos, mochila, colete = ""
    local ped = PlayerPedId()
    if GetEntityHealth(ped) > 100 then
        chapeu = "chapeu "..(GetPedPropIndex(ped, 0) > 0 and GetPedPropIndex(ped, 0) or "").." "..(GetPedPropTextureIndex(ped, 0) > 0 and GetPedPropTextureIndex(ped, 0) or "")
        oculos = "oculos "..(GetPedPropIndex(ped, 0) > 0 and GetPedPropIndex(ped, 0) or "").." "..(GetPedPropTextureIndex(ped, 0) > 0 and GetPedPropTextureIndex(ped, 0) or "")
        mascara = "mascara "..GetPedDrawableVariation(ped, 1).." "..GetPedTextureVariation(ped, 1)
        maos = "maos "..GetPedDrawableVariation(ped, 3).." "..GetPedTextureVariation(ped, 3)
        calca = "calca "..GetPedDrawableVariation(ped, 4).." "..GetPedTextureVariation(ped, 4)
        mochila = "mochila "..GetPedDrawableVariation(ped, 5).." "..GetPedTextureVariation(ped, 5)
        sapatos = "sapatos "..GetPedDrawableVariation(ped, 6).." "..GetPedTextureVariation(ped, 6)
        acessorios = "acessorios "..GetPedDrawableVariation(ped, 7).." "..GetPedTextureVariation(ped, 7)
        blusa = "blusa "..GetPedDrawableVariation(ped, 8).." "..GetPedTextureVariation(ped, 8)
        colete = "colete "..GetPedDrawableVariation(ped, 9).." "..GetPedTextureVariation(ped, 9)
        jaqueta = "jaqueta "..GetPedDrawableVariation(ped, 11).." "..GetPedTextureVariation(ped, 11)
        vRP.prompt("Preset:",chapeu.."; "..mascara.."; "..jaqueta.."; "..blusa.."; "..maos.."; "..calca.."; "..sapatos.."; "..acessorios.."; "..oculos.."; "..mochila.."; "..colete)
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- RENOMEAR PERSONAGEM
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('rename',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao")  then
        local idjogador = vRP.prompt(source, "Qual id do jogador?", "")
        local nome = vRP.prompt(source, "Novo nome", "")
        local firstname = vRP.prompt(source, "Novo sobrenome", "")
        local idade = vRP.prompt(source, "Nova idade", "")
        local identity = vRP.getUserIdentity(parseInt(idjogador))
        vRP.execute("vRP/update_user_identity",{
            user_id = idjogador,
            firstname = firstname,
            name = nome,
            age = idade,
            registration = identity.registration,
            phone = identity.phone
        })
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- ESTOQUE
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('estoque',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") then
        if args[1] and args[2] then
            vRP.execute("creative/set_estoque",{ vehicle = args[1], quantidade = args[2] })
            TriggerClientEvent("Notify",source,"sucesso","Voce colocou mais <b>"..args[2].."</b> no estoque, para o carro <b>"..args[1].."</b>.") 
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- ADD CAR
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('addcar',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local nplayer = vRP.getUserId(parseInt(args[2]))
    if vRP.hasPermission(user_id,"admin.permissao") then
        if args[1] and args[2] then
            local nuser_id = vRP.getUserId(nplayer)
            local identity = vRP.getUserIdentity(user_id)
            local identitynu = vRP.getUserIdentity(nuser_id)
            vRP.execute("creative/add_vehicle",{ user_id = parseInt(args[2]), vehicle = args[1], ipva = parseInt(os.time()) }) 
            TriggerClientEvent("Notify",source,"sucesso","Voce adicionou o veículo <b>"..args[1].."</b> para o Passaporte: <b>"..parseInt(args[2]).."</b>.") 
            SendWebhookMessage(webhooknovo,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[ADICIONOU]: "..args[1].." \n[PARA O ID]: "..nuser_id.." "..identitynu.name.." "..identitynu.firstname.." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```") 
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- REM CAR
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('remcar',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local nplayer = vRP.getUserId(parseInt(args[2]))
    if vRP.hasPermission(user_id,"admin.permissao") then
        if args[1] and args[2] then
            local nuser_id = vRP.getUserId(nplayer)
            local identity = vRP.getUserIdentity(user_id)
            local identitynu = vRP.getUserIdentity(nuser_id)
            vRP.execute("creative/rem_vehicle",{ user_id = parseInt(args[2]), vehicle = args[1], ipva = parseInt(os.time())  }) 
            TriggerClientEvent("Notify",source,"sucesso","Voce removeu o veículo <b>"..args[1].."</b> do Passaporte: <b>"..parseInt(args[2]).."</b>.") 
            SendWebhookMessage(webhooknovo,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[REMOVEU]: "..args[1].." \n[PARA O ID]: "..nuser_id.." "..identitynu.name.." "..identitynu.firstname.." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
        end
    end
end)
------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- UNCUFF
------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('uncuff',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if user_id then
		if vRP.hasPermission(user_id,"admin.permissao") then
			TriggerClientEvent("admcuff",source)
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- ITEM
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand("item",function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		vRP.upThirst(user_id)
		if vRP.hasPermission(user_id,"admin.permissao") then
			if args[1] and args[2] and vRP.itemNameList(args[1]) ~= nil then
				vRP.giveInventoryItem(user_id,args[1],parseInt(args[2]))
				SendWebhookMessage(webhookgive,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[PEGOU]: "..args[1].." \n[QUANTIDADE]: "..vRP.format(parseInt(args[2])).." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
			end
		end
	end
end)
------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUEL
------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('admfuel',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if user_id then
		if vRP.hasPermission(user_id,"admin.permissao") then
			TriggerClientEvent("admfuel",source)
		end	
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- cOBJECT
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('cobject',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if vRP.hasPermission(user_id,"admin.permissao") then
		TriggerClientEvent('cobject',source,args[1],args[2])
	end
end)
------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SYNCAREA
------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('clearall',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local x,y,z = vRPclient.getPosition(source)
    if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"polpar.permissao") then
        TriggerClientEvent("syncarea",-1,x,y,z)
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TROCAR SEXO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('skin',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") then
        if parseInt(args[1]) then
            local nplayer = vRP.getUserSource(parseInt(args[1]))
            if nplayer then
                TriggerClientEvent("skinmenu",nplayer,args[2])
                TriggerClientEvent("Notify",source,"sucesso","Voce setou a skin <b>"..args[2].."</b> no passaporte <b>"..parseInt(args[1]).."</b>.")
            end
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- CAR COLOR
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('carcolor',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") then
        local vehicle = vRPclient.getNearestVehicle(source,7)
        if vehicle then
            local rgb = vRP.prompt(source,"RGB Color(255 255 255):","")
            rgb = sanitizeString(rgb,"\"[]{}+=?!_()#@%/\\|,.",false)
            local r,g,b = table.unpack(splitString(rgb," "))
            TriggerClientEvent('vcolorv',source,vehicle,tonumber(r),tonumber(g),tonumber(b))
            
            TriggerClientEvent("Notify",source,"sucesso","Cor alterada")
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- TRYDELETEOBJ
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent("trydeleteobj")
AddEventHandler("trydeleteobj",function(index)
    TriggerClientEvent("syncdeleteobj",-1,index)
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- FIX
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('fix',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local vehicle = vRPclient.getNearestVehicle(source,11)
	if vehicle then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			TriggerClientEvent('reparar',source)
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- GODSTREAMER
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('god2',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
        if args[1] then
            local nplayer = vRP.getUserSource(parseInt(args[1]))
            if nplayer then
                vRPclient.killGod(nplayer)
                vRPclient.setHealth(nplayer,120)

                TriggerClientEvent("resetBleeding",nplayer)
                TriggerClientEvent("resetDiagnostic",nplayer)

            end
        else
            vRPclient.killGod(source)
            vRPclient.setHealth(source,120)

            TriggerClientEvent("resetBleeding",source)
            TriggerClientEvent("resetDiagnostic",source)
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- REVIVE
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('god',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
    if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
        if args[1] then
            local nplayer = vRP.getUserSource(parseInt(args[1]))
            if nplayer then
                vRPclient.killGod(nplayer)
                vRPclient.setHealth(nplayer,400)
				SendWebhookMessage(webhookadmin,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[DEU GOD NO ID:]: "..args[1].." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
            end
        else
            vRPclient.killGod(source)
			vRPclient.setHealth(source,400)
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- REVIVE ALL
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('godall',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") then
        local users = vRP.getUsers()
        for k,v in pairs(users) do
            local id = vRP.getUserSource(parseInt(k))
            if id then
            	vRPclient.killGod(id)
				vRPclient.setHealth(id,400)
				--print(id)
            end
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- KICK ALL
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('kickall',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") then
        local users = vRP.getUsers()
        for k,v in pairs(users) do
            local id = vRP.getUserSource(parseInt(k))
            if id then
                vRP.kick(id,"Você foi vitima do terremoto.")
            end
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- HASH
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('hash',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if vRP.hasPermission(user_id,"admin.permissao") then
		TriggerClientEvent('vehash',source)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- TUNING
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('tuning',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if vRP.hasPermission(user_id,"admin.permissao") then
		TriggerClientEvent('vehtuning',source)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- TUNING2
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('tuning2',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") then
		TriggerClientEvent('vehtuning2',source)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- WL
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('wl',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local identity = vRP.getUserIdentity(user_id)
    if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"aprovador.permissao") or vRP.hasPermission(user_id,"suporte.permissao") or vRP.hasPermission(user_id,"mod.permissao") then
        if args[1] then
            vRP.setWhitelisted(parseInt(args[1]),true)
            TriggerClientEvent("Notify",source,"sucesso","Voce aprovou o passaporte <b>"..args[1].."</b> na whitelist.")
            SendWebhookMessage(webhookadmin,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[APROVOU WL]: "..args[1].." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- TP ALL
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('tpall', function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if user_id ~= nil then
        local player = vRP.getUserSource(user_id)
        local x,y,z = vRPclient.getPosition(source)
        if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") then
            local users = vRP.getUsers()
            for k,v in pairs(users) do
                local target_source = vRP.getUserSource(k)
                local rsource = vRP.getUserSource(k)
                if rsource ~= source then
                    vRPclient.teleport(rsource,x,y,z)
                end
            end
        end
    end
end) 
-----------------------------------------------------------------------------------------------------------------------------------------
-- UNWL
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('unwl',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"suporte.permissao") or vRP.hasPermission(user_id,"mod.permissao")  then
		if args[1] then
			vRP.setWhitelisted(parseInt(args[1]),false)
			TriggerClientEvent("Notify",source,"sucesso","Voce retirou o passaporte <b>"..args[1].."</b> da whitelist.")
			SendWebhookMessage(webhookadmin,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[RETIROU WL]: "..args[1].." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- KICK
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('kick',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
		if args[1] then
			local id = vRP.getUserSource(parseInt(args[1]))
			if id then
				vRP.kick(id,"Você foi expulso da cidade.")
				TriggerClientEvent("Notify",source,"sucesso","Voce kickou o passaporte <b>"..args[1].."</b> da cidade.")
				SendWebhookMessage(webhookadmin,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[KICKOU]: "..args[1].." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- BAN
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('ban',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") then
		if args[1] then
			vRP.setBanned(parseInt(args[1]),true)
			TriggerClientEvent("Notify",source,"sucesso","Voce baniu o passaporte <b>"..args[1].."</b> da cidade.")
			SendWebhookMessage(webhookadmin,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[BANIU]: "..args[1].." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- UNBAN
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('unban',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if vRP.hasPermission(user_id,"admin.permissao") then
		if args[1] then
			vRP.setBanned(parseInt(args[1]),false)
			TriggerClientEvent("Notify",source,"sucesso","Voce desbaniu o passaporte <b>"..args[1].."</b> da cidade.")
			SendWebhookMessage(webhookadmin,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[DESBANIU]: "..args[1].." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- MONEY
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('money',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if vRP.hasPermission(user_id,"admin.permissao") then
		if args[1] then
			vRP.giveMoney(user_id,parseInt(args[1]))
			SendWebhookMessage(webhookadmin,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[FEZ]: $"..vRP.format(parseInt(args[1])).." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- NC
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('nc',function(source,args,rawCommand)
    local source = source
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") or vRP.hasPermission(user_id,"dandan.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
        if args[1] then
            local nplayer = vRP.getUserSource(parseInt(args[1]))
            if nplayer then
                vRPclient.toggleNoclip(nplayer)
            end
        else
            vRPclient.toggleNoclip(source)
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- TPCDS
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('tpcds',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") then
		local fcoords = vRP.prompt(source,"Cordenadas:","")
		if fcoords == "" then
			return
		end
		local coords = {}
		for coord in string.gmatch(fcoords or "0,0,0","[^,]+") do
			table.insert(coords,parseInt(coord))
		end
		vRPclient.teleport(source,coords[1] or 0,coords[2] or 0,coords[3] or 0)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- ARMA
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('arma',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if user_id then
        if args[1] then
            if vRP.hasPermission(user_id,"ceo.permissao") then
            vRPclient.giveWeapons(source,{[args[1]] = { ammo = 250 }})
            end
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- ACCOUNT
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('cc',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if user_id then
	  	if vRP.hasPermission(user_id,"admin.permissao") then
			if args[1] then
			 	local nuser_id = vRP.getUserId(parseInt(args[1]))
			 	local nsource = vRP.getUserSource(parseInt(args[1]))
			 	if nsource then
			   	vRP.prompt(source,"Informations:","Steam Hex: "..GetPlayerIdentifier(parseInt(nsource),0))
			   	if GetPlayerIdentifier(parseInt(nsource),4) then
				 	TriggerClientEvent("Notify",source,"importante","<b>Usuário:</b> "..args[1].." <br><b>License:</b> "..GetPlayerIdentifier(parseInt(nsource),1).."<br><b>Discord Id:</b> "..GetPlayerIdentifier(parseInt(nsource),4).."<br><b>Steam Hex:</b> "..GetPlayerIdentifier(parseInt(nsource),0),8000)
			   	else
				 	TriggerClientEvent("Notify",source,"importante","<b>Usuário:</b> "..args[1].." <br><b>License:</b> "..GetPlayerIdentifier(parseInt(nsource),1).."<br><b>Steam Hex:</b> "..GetPlayerIdentifier(parseInt(nsource),0),8000)
			  	end
			 	else
			   		TriggerClientEvent("Notify",source,"negado","Este jogador precisa estar online, para mais informações chame um desenvolvedor",8000)
			 	end
		  	end
	  	end
	end
end)
------------------------------------------------
--[ COORDENADAS ]-------------------------------
------------------------------------------------
RegisterCommand('cds',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"owner.permissao") or vRP.hasPermission(user_id,"admin.permissao") then
        local x,y,z = vRPclient.getPosition(source)
        heading = GetEntityHeading(GetPlayerPed(-1))
        vRP.prompt(source,"Cordenadas:","['x'] = "..tD(x)..", ['y'] = "..tD(y)..", ['z'] = "..tD(z))
    end
end)

RegisterCommand('cds2',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"owner.permissao") or vRP.hasPermission(user_id,"admin.permissao") then
        local x,y,z = vRPclient.getPosition(source)
        vRP.prompt(source,"Cordenadas:",tD(x)..", "..tD(y)..", "..tD(z))
    end
end)

RegisterCommand('cds3',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"owner.permissao") or vRP.hasPermission(user_id,"admin.permissao") then
        local x,y,z = vRPclient.getPosition(source)
        vRP.prompt(source,"Cordenadas:","{name='ATM', id=277, x="..tD(x)..", y="..tD(y)..", z="..tD(z).."},")
    end
end)

RegisterCommand('cds4',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"owner.permissao") or vRP.hasPermission(user_id,"admin.permissao") then
        local x,y,z = vRPclient.getPosition(source)
        vRP.prompt(source,"Cordenadas:","x = "..tD(x)..", y = "..tD(y)..", z = "..tD(z))
    end
end)

function tD(n)
    n = math.ceil(n * 100) / 100
    return n
end
-----------------------------------------------------------------------------------------------------------------------------------------
-- TD
-----------------------------------------------------------------------------------------------------------------------------------------
function tD(n)
    n = math.ceil(n*100)/100
    return n
end
-----------------------------------------------------------------------------------------------------------------------------------------
-- CDS
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('coords',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if user_id then
		if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") then
			local x,y,z = vRPclient.getPosition(source)
			vRP.prompt(source,"Cordenadas:",mathLegth(x)..","..mathLegth(y)..","..mathLegth(z))
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- MATHLEGTH
-----------------------------------------------------------------------------------------------------------------------------------------
function mathLegth(n)
	n = math.ceil(n*100)/100
	return n
end
-----------------------------------------------------------------------------------------------------------------------------------------
-- GROUP
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('group',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if vRP.hasPermission(user_id,"admin.permissao") then
		if args[1] and args[2] then
			vRP.addUserGroup(parseInt(args[1]),args[2])
			TriggerClientEvent("Notify",source,"sucesso","Voce setou o passaporte <b>"..parseInt(args[1]).."</b> no grupo <b>"..args[2].."</b>.")
			SendWebhookMessage(webhooksetar,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: "..args[2].." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- UNGROUP
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('ungroup',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if vRP.hasPermission(user_id,"admin.permissao") then
		if args[1] and args[2] then
			vRP.removeUserGroup(parseInt(args[1]),args[2])
			TriggerClientEvent("Notify",source,"sucesso","Voce removeu o passaporte <b>"..parseInt(args[1]).."</b> do grupo <b>"..args[2].."</b>.")
			SendWebhookMessage(webhooktirarset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[REMOVEU]: "..args[1].." \n[GRUPO]: "..args[2].." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- TPTOME
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('tptome',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") then
		if args[1] then
			local tplayer = vRP.getUserSource(parseInt(args[1]))
			local x,y,z = vRPclient.getPosition(source)
			if tplayer then
				vRPclient.teleport(tplayer,x,y,z)
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- TPTO
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('tpto',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
		if args[1] then
			local tplayer = vRP.getUserSource(parseInt(args[1]))
			if tplayer then
				vRPclient.teleport(source,vRPclient.getPosition(tplayer))
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- TPWAY
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('tpway',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
		TriggerClientEvent('tptoway',source)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- CAR
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('car',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"vendedor.permissao") or vRP.hasPermission(user_id,"mod.permissao") then
		if args[1] then
			TriggerClientEvent('spawnarveiculo',source,args[1])
			SendWebhookMessage(webhookcarro,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SPAWNOU]: "..(args[1]).." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- DELNPCS
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('delnpcs',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if vRP.hasPermission(user_id,"admin.permissao") then
		TriggerClientEvent('delnpcs',source)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- PLAYERSON
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('pon',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"dandan.permissao") or vRP.hasPermission(user_id,"mod.permissao") or vRP.hasPermission(user_id,"sup.permissao") then
        local users = vRP.getUsers()
        local players = ""
		local quantidade = 0
		
        for k,v in pairs(users) do
            if k ~= #users then
                players = players
			end
			
            players = players.." "..k
            quantidade = quantidade + 1
		end
		
        TriggerClientEvent('chatMessage',source,"TOTAL ONLINE",{255,160,0},quantidade)
        TriggerClientEvent('chatMessage',source,"ID's ONLINE",{255,160,0},players)
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- PARAMEDICO
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setpar',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"PaisanaParamedico")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: PaisanaParamedico "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- POLICIA
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setpol',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"PaisanaPolicia")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: PaisanaPolicia "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- MECANICO
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setmec',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"PaisanaMecanico")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: PaisanaMecanico "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- VAGOS
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setvagos',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"Vagos")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: Vagos "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- BALLAS
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setballas',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"Ballas")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: Ballas "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- GROOVE
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setgrove',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"Groove")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: Groove "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- CRIPS
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setcrips',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"Crips")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: crips "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- Bloods
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setbloods',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"Bloods")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: Bloods "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- LIFEINVADER
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setlife',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"Lifeinvader")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: Lifeinvader "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- BAHAMAS
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setbahamas',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"Bahamas")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: bahamas "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- SICILIANA
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setsic',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"siciliana")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: sovietica "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- TRIADE
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('settriade',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"Triade")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: albanesa "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- BRATIVA
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('setbri',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if user_id then
		if vRP.hasGroup(user_id, "Admin") or vRP.hasGroup(user_id,"mod.permissao") or vRP.hasPermission(user_id,"suporte.permissao") then
			if args[1] then
				local nplayer = vRP.getUserSource(parseInt(args[1]))
				if nplayer then
					vRP.addUserGroup(parseInt(args[1]),"Britanica")
					TriggerClientEvent("Notify",source,"sucesso","Setagem concluida com sucesso.",8000)
					SendWebhookMessage(webhookset,"```prolog\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]: "..args[1].." \n[GRUPO]: Britanica "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r```")
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- CHAT ADMIN
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('aa',function(source,args,rawCommand)
	if args[1] then
		local user_id = vRP.getUserId(source)
		local identity = vRP.getUserIdentity(user_id)
		local permission = "suporte.permissao"
		if vRP.hasPermission(user_id,permission) then
			local soldado = vRP.getUsersByPermission(permission)
			for l,w in pairs(soldado) do
				local player = vRP.getUserSource(parseInt(w))
				if player then
					async(function()
						TriggerClientEvent('chatMessage',player,"[CHAT ADMIN] ["..user_id.."] "..identity.name.." "..identity.firstname,{0,242,212},rawCommand:sub(3))
					end)
				end
			end
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- VIPS
----------------------------------------------------------------------------------------------------------------------------------------
 RegisterCommand('vips', function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
	local hr = vRP.getUsersByPermission("hire.permissao")
	local spr = vRP.getUsersByPermission("flame.permissao")
	local ult = vRP.getUsersByPermission("ultimate.permissao")
	local paramedicos = 0
	local oficiais_nomes = ""
    if args[1] == "Rental" and vRP.hasPermission(user_id,"suporte.permissao") then
		for k,v in ipairs(hr) do
			local identity = vRP.getUserIdentity(parseInt(v))
			oficiais_nomes = oficiais_nomes .. "<b>" .. v .. "</b>: " .. identity.name .. " " .. identity.firstname .. "<br>"
			paramedicos = paramedicos + 1
		end
		TriggerClientEvent("Notify",source,"importante", "Há <b>"..paramedicos.." Vip(s) Rental</b> na cidade.")
		if parseInt(paramedicos) > 0 then
			TriggerClientEvent("Notify",source,"importante", oficiais_nomes)
		end
	elseif args[1] == "Ice" and vRP.hasPermission(user_id,"suporte.permissao") then
		for k,v in ipairs(spr) do
			local identity = vRP.getUserIdentity(parseInt(v))
			oficiais_nomes = oficiais_nomes .. "<b>" .. v .. "</b>: " .. identity.name .. " " .. identity.firstname .. "<br>"
			paramedicos = paramedicos + 1
		end
		TriggerClientEvent("Notify",source,"importante", "Há <b>"..paramedicos.." Vip(s) Ice</b> na cidade.")
		if parseInt(paramedicos) > 0 then
			TriggerClientEvent("Notify",source,"importante", oficiais_nomes)
		end
	elseif args[1] == "Ultimate" and vRP.hasPermission(user_id,"suporte.permissao") then
		for k,v in ipairs(ult) do
			local identity = vRP.getUserIdentity(parseInt(v))
			oficiais_nomes = oficiais_nomes .. "<b>" .. v .. "</b>: " .. identity.name .. " " .. identity.firstname .. "<br>"
			paramedicos = paramedicos + 1
		end
		TriggerClientEvent("Notify",source,"importante", "Há <b>"..paramedicos.." Vip(s) Ultimate</b> na cidade.")
		if parseInt(paramedicos) > 0 then
			TriggerClientEvent("Notify",source,"importante", oficiais_nomes)
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- KILL
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('kill',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
    if vRP.hasPermission(user_id,"admin.permissao") then
        if args[1] then
            local nplayer = vRP.getUserSource(parseInt(args[1]))
            if nplayer then
                vRPclient.setHealth(nplayer,0)
				TriggerClientEvent("Notify",source,"importante","Você matou o passaporte "..args[1])
			end
		else
			args[1] = user_id
            vRPclient.setHealth(source,0)
		end	
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- ANUNCIO ADM
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('adm',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if user_id then
		if vRP.hasPermission(user_id,"suporte.permissao") then
			local mensagem = vRP.prompt(source,"Mensagem:","")
			--local nome = vRP.prompt(source,"Nome:","")
			local after = "Mensagem enviada pela Staff"
			if mensagem == "" or nome == "" then
				return
			end
			TriggerClientEvent("Notify",-1,"importante",mensagem, after)
		end
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- /SPEC Rusher#1337 SERVER
----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand("spec",function(source,args)
    local source = source
    local user_idDoJogadorParaSpectar = tonumber(args[1])
    local user_idDoJogadorQueExecutouoCOMANDO = vRP.getUserId(source)
    if vRP.hasPermission(user_idDoJogadorQueExecutouoCOMANDO,"admin.permissao") or  vRP.hasPermission(user_idDoJogadorQueExecutouoCOMANDO,"mod.permissao") then
        local sourceDoJogadorParaSpectar = vRP.getUserSource(user_idDoJogadorParaSpectar)
        if true then
            TriggerClientEvent("SpecMode", source,sourceDoJogadorParaSpectar)
        --else
            --TriggerClientEvent("Notify", source, "Negado", "Esse player não está online...")
        end
    end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- ADD CASA
-----------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('addcasa',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") then
        if args[1] and args[2] then
            local nuser_id = parseInt(args[1])
            local identity = vRP.getUserIdentity(user_id)
            local identitynu = vRP.getUserIdentity(nuser_id)
            vRP.execute("homes/rem_allpermissions",{ home = tostring(args[2]) })
            vRP.execute("homes/buy_permissions",{ home = tostring(args[2]), user_id = nuser_id, tax = os.time() })
            TriggerClientEvent("Notify",source,"sucesso","Voce adicionou a casa <b>"..args[2].."</b> para o Passaporte: <b>"..parseInt(args[1]).."</b>.") 
            SendWebhookMessage(webhookcasas,"```ini\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[ADICIONOU A CASA]: "..args[2].." \n[PARA O ID]: "..nuser_id.." "..identitynu.name.." "..identitynu.firstname.." "..os.date("\n[Data]: %d/%m/%Y [Hora]: %H:%M:%S").." \r``` <@310606304995442690> <@290346669294354434>") 
        end
    end
end)
------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUEL
------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('admfuel',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	if user_id then
		if vRP.hasPermission(user_id,"admin.permissao") then
			TriggerClientEvent("admfuel",source)
		end	
	end
end)

------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUEL
------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterCommand('admfueln',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if user_id then
        if vRP.hasPermission(user_id,"admin.permissao") then
            TriggerClientEvent("admfuel",source)
        end 
    end
end)

--------------------------------------Fogo---------------------------------
RegisterCommand('fogo', function(source)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") then
        TriggerClientEvent('FOGO', source)
    end
end)

-----------------------------------------------------------------ORGS--------------------------------------------------------
RegisterCommand('orgs', function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local player = vRP.getUserSource(user_id)
    local oficiais = vRP.getUsersByPermission("policia.permissao") 
    local policia = 0
	local oficiais_nomes = ""
	----------------------------------------------------------------------
	local paramedicos = vRP.getUsersByPermission("paramedico.permissao")
    local hp = 0
	local paramedicos_nomes = ""
	----------------------------------------------------------------------
	local mecanicos = vRP.getUsersByPermission("mecanico.permissao")
    local mec = 0
	local mecanicos_nomes = ""
	----------------------------------------------------------------------
    if vRP.hasPermission(user_id,"ceo.permissao") or vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") or vRP.hasPermission(user_id,"sup.permissao")  then
		
		for k,v in ipairs(oficiais) do
            oficiais_nomes = oficiais_nomes .. "" .. v .. ", "
            policia = policia + 1
		end

		for k2,v2 in ipairs(paramedicos) do
            paramedicos_nomes = paramedicos_nomes .. "" .. v2 .. ", "
            hp = hp + 1
		end

		for k3,v3 in ipairs(mecanicos) do
            mecanicos_nomes = mecanicos_nomes .. "" .. v3 .. ", "
            mec = mec + 1
		end
		
        if parseInt(policia) > -1 then
			TriggerClientEvent('chatMessage',source,"ID's POLICIAIS",{66,150,245},oficiais_nomes)
			TriggerClientEvent('chatMessage',source,"ID's PARAMEDICOS",{245,87,66},paramedicos_nomes)
			TriggerClientEvent('chatMessage',source,"ID's MECANICOS",{245,209,66},mecanicos_nomes)
		end
    end
end)

------------------------------------------------------------------fACS---------------------------------------------------
RegisterCommand('facs', function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    local player = vRP.getUserSource(user_id)
    local ballas = vRP.getUsersByPermission("ballas.permissao")
    local rxs = 0
	local ballas_nomes = ""
	----------------------------------------------------------------------
	local grove = vRP.getUsersByPermission("grove.permissao")
    local vrd = 0
	local grove_nomes = ""
	----------------------------------------------------------------------
	local vagos = vRP.getUsersByPermission("vagos.permissao")
    local amrls = 0
	local vagos_nomes = ""
	----------------------------------------------------------------------
	local bloods = vRP.getUsersByPermission("blood.permissao")
    local lrjs = 0
	local bloods_nomes = ""
	----------------------------------------------------------------------
	local crips = vRP.getUsersByPermission("crips.permissao")
    local mfa = 0
	local crips_nomes = ""
	--[[ ----------------------------------------------------------------------
	local galaxy = vRP.getUsersByPermission("galaxy.permissao")
    local glx = 0
	local galaxy_nomes = "" ]]
	----------------------------------------------------------------------albanesa
	local albanesa = vRP.getUsersByPermission("albanesa.permissao")
    local dcs = 0
	local albanesa_nomes = ""
	----------------------------------------------------------------------
	local life = vRP.getUsersByPermission("lifeinvader.permissao")
    local lfe = 0
	local life_nomes = ""
	----------------------------------------------------------------------
	local sovietica = vRP.getUsersByPermission("mafia.permissao")
    local mtc = 0
	local sovietica_nomes = ""
	----------------------------------------------------------------------
	local bahamas = vRP.getUsersByPermission("bahamas.permissao")
    local bhm = 0
	local bahamas_nomes = ""
	----------------------------------------------------------------------
	--[[local britanica = vRP.getUsersByPermission("britanica.permissao")
	local frous = 0
	local britanica_nomes = ""]]
	----------------------------------------------------------------------
	if vRP.hasPermission(user_id,"ceo.permissao") or vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") or vRP.hasPermission(user_id,"sup.permissao")  then

		
		for k,v in ipairs(ballas) do
            ballas_nomes = ballas_nomes .. "" .. v .. ", "
            rxs = rxs + 1
		end

		for k2,v2 in ipairs(grove) do
            grove_nomes = grove_nomes .. "" .. v2 .. ", "
            vrd = vrd + 1
		end

		for k3,v3 in ipairs(vagos) do
            vagos_nomes = vagos_nomes .. "" .. v3 .. ", "
            amrls = amrls + 1
		end

		for k4,v4 in ipairs(bloods) do
            bloods_nomes = bloods_nomes .. "" .. v4 .. ", "
            lrjs = lrjs + 1
		end

		for k5,v5 in ipairs(crips) do
            crips_nomes = crips_nomes .. "" .. v5 .. ", "
            mfa = mfa + 1
		end

		--[[ for k6,v6 in ipairs(galaxy) do
            galaxy_nomes = galaxy_nomes .. "" .. v6 .. ", "
            glx = glx + 1
		end ]]

		for k7,v7 in ipairs(albanesa) do
            albanesa_nomes = albanesa_nomes .. "" .. v7 .. ", "
            dcs = dcs + 1
		end

		for k8,v8 in ipairs(life) do
            life_nomes = life_nomes .. "" .. v8 .. ", "
            lfe = lfe + 1
		end

		for k9,v9 in ipairs(sovietica) do
            sovietica_nomes = sovietica_nomes .. "" .. v9 .. ", "
            mtc = mtc + 1
		end
		
		for k13,v13 in ipairs(bahamas) do
            bahamas_nomes = bahamas_nomes .. "" .. v13 .. ", "
            bhm = bhm + 1
		end

		--[[for k11,v11 in ipairs(britanica) do
            britanica_nomes = britanica_nomes .. "" .. v11 .. ", "
            frous = frous + 1
		end]]


        if parseInt(roxos) > -1 then
			TriggerClientEvent('chatMessage',source,"ID's BALLAS",{170, 66, 245},ballas_nomes)
			TriggerClientEvent('chatMessage',source,"ID's GROOVE",{66, 245, 72},grove_nomes)
			TriggerClientEvent('chatMessage',source,"ID's VAGOS",{245,230,66},vagos_nomes)
			TriggerClientEvent('chatMessage',source,"ID's BLOODS",{115, 21, 20},bloods_nomes)
			TriggerClientEvent('chatMessage',source,"ID's CRIPS",{16, 108, 158},crips_nomes)
			--[[ TriggerClientEvent('chatMessage',source,"ID's GALAXY",{88, 6, 117},galaxy_nomes) ]]
			TriggerClientEvent('chatMessage',source,"ID's ALBANESA",{255, 255, 255},albanesa_nomes)
			TriggerClientEvent('chatMessage',source,"ID's LIFE INVADER",{255, 0, 0},life_nomes)
			TriggerClientEvent('chatMessage',source,"ID's SOVIETICA",{204, 202, 202},sovietica_nomes)
			TriggerClientEvent('chatMessage',source,"ID's BAHAMAS",{224, 43, 227},bahamas_nomes)
			--TriggerClientEvent('chatMessage',source,"ID's BRITANICA",{227, 211, 172	},britanica_nomes)
		end
    end
end)
--------------------------------------------------------------------------kill2-----------------------------------------------------
RegisterCommand('kill2',function(source,args,rawCommand)
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"dandan.permissao") or vRP.hasPermission(user_id,"mod.permissao") then
        if args[1] then
            local nplayer = vRP.getUserSource(parseInt(args[1]))
            if nplayer then
                vRPclient.killGod(nplayer)
                vRPclient.setHealth(nplayer,100)
            end
        else
            vRPclient.killGod(source)
            vRPclient.setHealth(source,150)
            vRPclient.setArmour(source,0)
        end
    end
end)

--[ REVIVER ]-----------------------------------------------------------------------------------------------------------------

RegisterCommand('reviver',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)

    if vRP.hasPermission(user_id,"admin.permissao") or vRP.hasPermission(user_id,"mod.permissao") or vRP.hasPermission(user_id,"dandan.permissao") or vRP.hasPermission(user_id,"sup.permissao") then
        if args[1] then
			local nplayer = vRP.getUserSource(parseInt(args[1]))
			if nplayer then
				local nuser_id = vRP.getUserId(nplayer)
				local identitynu = vRP.getUserIdentity(nuser_id)

				PerformHttpRequest(logAdminReviver, function(err, text, headers) end, 'POST', json.encode({
					embeds = {
						{ 	------------------------------------------------------------
							title = "REGISTRO DE REVIVER⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n⠀",
							thumbnail = {
								url = "https://cdn.discordapp.com/attachments/786333161487073280/786334221002211398/dntz1_ldepartamento.png"
							}, 
							fields = {
								{ 
									name = "**COLABORADOR DA EQUIPE:**",
									value = "**"..identity.name.." "..identity.firstname.."** [**"..user_id.."**]\n⠀"
								},
								{
									name = "**INFORMAÇÕES DO PLAYER REVIVIDO:**",
									value = "**"..identitynu.name.." "..identitynu.firstname.."** [**"..nuser_id.."**]\n⠀"
								}
							}, 
							footer = { 
								text = "GHRP - "..os.date("%d/%m/%Y | %H:%M:%S"),
								icon_url = "https://cdn.discordapp.com/attachments/786333161487073280/786333949898391592/512_gh.png"
							},
							color = 15906321 
						}
					}
				}), { ['Content-Type'] = 'application/json' })

                vRPclient.killGod(nplayer)
				vRPclient.setHealth(nplayer,110)
            end
		else
			PerformHttpRequest(logAdminReviver, function(err, text, headers) end, 'POST', json.encode({
				embeds = {
					{ 	------------------------------------------------------------
						title = "REGISTRO DE REVIVER⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n⠀",
						thumbnail = {
							url = "https://cdn.discordapp.com/attachments/786333161487073280/786334221002211398/dntz1_ldepartamento.png"
						}, 
						fields = {
							{ 
								name = "**COLABORADOR DA EQUIPE:**",
								value = "**"..identity.name.." "..identity.firstname.."** [**"..user_id.."**]\n⠀"
							}
						}, 
						footer = { 
							text = "GHRP - "..os.date("%d/%m/%Y | %H:%M:%S"),
							icon_url = "https://cdn.discordapp.com/attachments/786333161487073280/786333949898391592/512_gh.png"
						},
						color = 15906321 
					}
				}
			}), { ['Content-Type'] = 'application/json' })

            vRPclient.killGod(source)
			vRPclient.setHealth(source,110)
        end
    end
end)

RegisterCommand('setarpolicia',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if vRP.hasPermission(user_id,"policiaftu.permissao") then
	vRP.request(source,"Você deseja ser contratado?",30)
		if args[1] then
			SendWebhookMessage(webhookfac,"```css\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[SETOU]:"..parseInt(args[1]).." \n[GRUPO]: Policia \nData e Hora : "..os.date("%d/%m/%Y %H:%M:%S").." \r```")
			vRP.addUserGroup(parseInt(args[1]),"PaisanaPolicia")
		end
	end
end)

RegisterCommand('removerpolicia',function(source,args,rawCommand)
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(user_id)
	if vRP.hasPermission(user_id,"policiaftu.permissao") then
		if args[1] then
			SendWebhookMessage(webhookfac,"```css\n[ID]: "..user_id.." "..identity.name.." "..identity.firstname.." \n[Removeu]:"..parseInt(args[1]).." \n[GRUPO]: Policia \nData e Hora : "..os.date("%d/%m/%Y %H:%M:%S").." \r```")
			vRP.removeUserGroup(parseInt(args[1]),"Policia")
			vRP.removeUserGroup(parseInt(args[1]),"PaisanaPolicia")
		end
	end
end)
