 fx_version "bodacious"
game "gta5"

ui_page_preload "yes"
ui_page "index.html"

client_scripts {
	"client.lua"
}

files {
	"index.html"
}
shared_script "@ThnAC/natives.lua"
client_script "@ThnAC/stopper.lua"