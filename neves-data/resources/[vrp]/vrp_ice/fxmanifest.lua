fx_version 'bodacious'
game 'gta5'

client_scripts {
	'recoil.lua',
	'iploader.lua',
	'client.lua',
	'helicam.lua',
	'pedsprofissoes.lua',
	'cl_teleports.lua',
	'dispatch.lua',
	'removehud.lua',
	'showeapons.lua'
}

