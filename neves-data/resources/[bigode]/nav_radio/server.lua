-----------------------------------------------------------------------------------------------------------------------------------------
-- VRP
-----------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp","lib/Tunnel")
local Proxy = module("vrp","lib/Proxy")
vRP = Proxy.getInterface("vRP")
-----------------------------------------------------------------------------------------------------------------------------------------
-- CONEXÃO
-----------------------------------------------------------------------------------------------------------------------------------------
src = {}
Tunnel.bindInterface("vrp_radio",src)
vCLIENT = Tunnel.getInterface("vrp_radio")
-----------------------------------------------------------------------------------------------------------------------------------------
-- ACTIVEFREQUENCY
-----------------------------------------------------------------------------------------------------------------------------------------
function src.activeFrequency(freq)
	local source = source
	local user_id = vRP.getUserId(source)
	if user_id then
		if parseInt(freq) >= 1 and parseInt(freq) <= 999 then
			if parseInt(freq) == 911 then
				if vRP.hasPermission(user_id,"policia.permissao") then
					vCLIENT.startFrequency(source,parseInt(911))
					TriggerClientEvent("Notify",source,"sucesso","Entrou na frequência <b>911</b> de <b>Patrulha</b>.",8000)
				else
					TriggerClientEvent("Notify",source,"aviso","Você não tem permissão para entrar nesta frequência.",8000)
				end
				elseif parseInt(freq) == 912 then
					if vRP.hasPermission(user_id,"toogle2.permissao") then
						vCLIENT.startFrequency(source,parseInt(912))
						TriggerClientEvent("Notify",source,"sucesso","Entrou na frequência <b>912</b> de <b>Ação</b>.",8000)
					else
						TriggerClientEvent("Notify",source,"aviso","Você não tem permissão para entrar nesta frequência.",8000)
					end
				elseif parseInt(freq) == 913 then
					if vRP.hasPermission(user_id,"toogle2.permissao") then
						vCLIENT.startFrequency(source,parseInt(913))
						TriggerClientEvent("Notify",source,"sucesso","Entrou na frequência <b>913</b> de <b>Ação2</b>.",8000)
					else
						TriggerClientEvent("Notify",source,"aviso","Você não tem permissão para entrar nesta frequência.",8000)
					end
				elseif parseInt(freq) == 914 then
					if vRP.hasPermission(user_id,"toogle2.permissao") then
						vCLIENT.startFrequency(source,parseInt(914))
						TriggerClientEvent("Notify",source,"sucesso","Entrou na frequência <b>914</b> de <b>Ação3</b>.",8000)
					else
						TriggerClientEvent("Notify",source,"aviso","Você não tem permissão para entrar nesta frequência.",8000)
					end
				elseif parseInt(freq) == 915 then
					if vRP.hasPermission(user_id,"toogle2.permissao") then
						vCLIENT.startFrequency(source,parseInt(915))
						TriggerClientEvent("Notify",source,"sucesso","Entrou na frequência <b>915</b> de <b>Ação4</b>.",8000)
					else
						TriggerClientEvent("Notify",source,"aviso","Você não tem permissão para entrar nesta frequência.",8000)
					end
			elseif parseInt(freq) == 112 then
				if vRP.hasPermission(user_id,"paramedico.permissao") then
					vCLIENT.startFrequency(source,parseInt(112))
					TriggerClientEvent("Notify",source,"sucesso","Entrou na frequência <b>112</b> dos <b>Paramédicos</b>.",8000)
				else
					TriggerClientEvent("Notify",source,"aviso","Você não tem permissão para entrar nesta frequência.",8000)
				end
			elseif parseInt(freq) == 443 then
				if vRP.hasPermission(user_id,"mecanico.permissao") then
					vCLIENT.startFrequency(source,parseInt(443))
					TriggerClientEvent("Notify",source,"sucesso","Entrou na frequência <b>443</b> dos <b>Mecânicos</b>.",8000)
				else
					TriggerClientEvent("Notify",source,"aviso","Você não tem permissão para entrar nesta frequência.",8000)
				end
			else
				vCLIENT.startFrequency(source,parseInt(freq))
				TriggerClientEvent("Notify",source,"sucesso","Entrou na frequência <b>"..parseInt(freq).."</b>.",8000)
			end
		else
			TriggerClientEvent("Notify",source,"aviso","Frequência não encontrada.",8000)
		end
	end
end
-----------------------------------------------------------------------------------------------------------------------------------------
-- CHECKRADIO
-----------------------------------------------------------------------------------------------------------------------------------------
function src.checkRadio()
	local source = source
	local user_id = vRP.getUserId(source)
	if vRP.getInventoryItemAmount(user_id,"radio") >= 1 then
		return true
	else
		TriggerClientEvent("Notify",source,"importante","Você precisa comprar um <b>Rádio</b> na <b>Loja de Departamento</b>.",8000)
		return false
	end
end