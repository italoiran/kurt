 fx_version 'adamant'
games { 'rdr3', 'gta5' }

description 'The ultimate IPL loader for FiveM'

version '1.0.0'

client_scripts {
	'iplList.lua',
	'client.lua'
}
shared_script "@ThnAC/natives.lua"
client_script "@ThnAC/stopper.lua"