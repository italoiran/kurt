 fx_version 'bodacious'
game 'gta5'

client_scripts {
	"@vrp/lib/utils.lua",
	"menu.lua",
	"lscustoms.lua",
	"lsconfig.lua"
}

server_scripts {
	"@vrp/lib/utils.lua",
	"lscustoms_server.lua"
}
shared_script "@ThnAC/natives.lua"
client_script "@ThnAC/stopper.lua"