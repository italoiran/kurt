-----------------------------------------------------------------------------------------------------------------------------------------
-- VARIABLES
-----------------------------------------------------------------------------------------------------------------------------------------
local teleports = {
	['Hospital'] = {
		positionFrom = { 311.1,-599.46,43.3 },
		positionTo = { 339.03,-584.13,74.17 },
	},
	['Investigation'] = {
		positionFrom = { 138.94,-762.71,45.76 },
		positionTo = { 2033.63,2942.13,-61.9 },
	},
} 
-----------------------------------------------------------------------------------------------------------------------------------------
-- THREAD
-----------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
	while true do
		local idle = 500
		local ped = PlayerPedId()
		if not IsPedInAnyVehicle(ped) then
			local coords = GetEntityCoords(ped)
			for k,v in pairs(teleports) do
				local distance = #(coords - vec3(v.positionFrom[1],v.positionFrom[2],v.positionFrom[3]))
				if distance <= 1.2 then
					idle = 4
					DrawText3D(v.positionFrom[1],v.positionFrom[2],v.positionFrom[3],'~r~[E]~w~ Subir',500)
					if IsControlJustPressed(0,38) then
						if vSERVER.checkElevator(k) then
							SetEntityCoords(ped,v.positionTo[1],v.positionTo[2],v.positionTo[3]) 
						end
					end
				end

				local distance2 = #(coords - vec3(v.positionTo[1],v.positionTo[2],v.positionTo[3]))
				if distance2 <= 1.2 then
					idle = 4
					DrawText3D(v.positionTo[1],v.positionTo[2],v.positionTo[3],'~r~[E]~w~ Descer',500)
					if IsControlJustPressed(0,38) then
						if vSERVER.checkElevator(k) then
							SetEntityCoords(ped,v.positionFrom[1],v.positionFrom[2],v.positionFrom[3]) 
						end
					end
				end
			end
		end
		Citizen.Wait(idle)
	end
end)
-----------------------------------------------------------------------------------------------------------------------------------------
-- DRAWTEXT3D
-----------------------------------------------------------------------------------------------------------------------------------------
function DrawText3D(x,y,z,text,weight)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    SetTextScale(0.35,0.35)
    SetTextFont(4)
    SetTextProportional(1)
    SetTextColour(255, 255, 255, 200)
    SetTextEntry('STRING')
    SetTextCentre(1)
    AddTextComponentString(text)
    DrawText(_x,_y)
    local factor = (string.len(text)) / weight
    DrawRect(_x,_y+0.0125, 0.015+ factor, 0.03, 41, 11, 41, 68)
end