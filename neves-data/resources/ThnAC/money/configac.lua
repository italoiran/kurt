config = {
	segundos = 15, -- Quantidade de segundos que irá resetar
	quantidade_maxima = 10, -- Quantidade maxima que pode givar money a cada 15 segundos
	permissao_imune = 'admin.permissao', -- Permissão que poderá givar money a vontade
	mensagem = 'VAI SPAWNAR MONEY NA CASA DO KARALHO!',
	log_registro = 'SUA WEBHOOK',
	log_suspeito = 'SUA WEBHOOK'
}

scripts = {
	{['script'] = 'emp_motorista', ['quantia_maxima'] = 900},
	{['script'] = 'emp_ifood', ['quantia_maxima'] = 1800},
	{['script'] = 'lixeiro_despejar', ['quantia_maxima'] = 360},
	{['script'] = 'emp_taxista', ['quantia_maxima'] = 800},
	{['script'] = 'farm_driftking', ['quantia_maxima'] = 0},
    {['script'] = 'vrp_lavagemfac', ['quantia_maxima'] = 50000},
	{['script'] = 'nui_24market', ['quantia_maxima'] = 5100},
	{['script'] = 'nui_gunshop', ['quantia_maxima'] = 5100},
	{['script'] = 'nui_gunshop', ['quantia_maxima'] = 5100},
	{['script'] = 'vrp_admin', ['quantia_maxima'] = 999999999},
	{['script'] = 'vrp', ['quantia_maxima'] = 9999999999},
	{['script'] = 'vrp_player', ['quantia_maxima'] = 10000000},
	{['script'] = 'vrp_policia', ['quantia_maxima'] = 5000},
}