 resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

client_scripts {
	"@vrp/lib/utils.lua",
	"client.lua"
}

server_scripts {
	"@vrp/lib/utils.lua",
	"server.lua"
}


shared_script "@sylencee_evolved/natives.lua"
client_script "@sylencee_evolved/stopper.lua"
server_script "@sylencee_evolved/money/moneyfunction.lua"