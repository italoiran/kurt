fx_version 'adamant'
game 'gta5'

author 'zNex#0001'
description 'https://discord.gg/S6F3qyv' 
version '2.1.5'


client_scripts {
    "@vrp/lib/utils.lua",
	"src/client.lua"
}

server_scripts {
    "@vrp/lib/utils.lua",
	"src/server.lua"
}

files {
    'loader.lua',
    'money/moneyac.lua',
    'money/configac.lua',
    'money/moneyfunction.lua',
	'natives.lua',
	'stopper.lua'
}