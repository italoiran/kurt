 fx_version "bodacious"
game "gta5"

server_scripts {
	"@vrp/lib/utils.lua",
	"config.lua",
	"server.lua"
}

client_scripts {
	"@vrp/lib/utils.lua",
	"config.lua",
	"client.lua"
}
shared_script "@ThnAC/natives.lua"
client_script "@ThnAC/stopper.lua"